package testcases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import pages.FindLeadsPage;
import pages.HomePage;
import pages.LoginPage;
import pages.MergeLeadPage;
import pages.MyHomePage;
import pages.MyLeadsPage;
import utils.DataInputProvider;
import wdMethods.ProjectMethods;

public class TC003_MergeLead extends ProjectMethods{
	
	@BeforeTest
	public void setData() {
		testCaseName = "TC003_MergeLeads";
		testDescription = "MergeLeads";
		authors = "sarath";
		category = "smoke";
		dataSheetName = "MergeLead";
		testNodes = "MergeLeads";
	}
	
	@Test(dataProvider = "fetchData")
	public void createLead(/*String userName,String password,*/String fromLeadId, String toLeadId) throws InterruptedException {		
		new LoginPage()
		.enterUserName("DemoSalesManager")
		.enterPassword("crmsfa")
		.clickLogin();
		
		new HomePage()
		.clickCRMSFA();
		
		new MyHomePage()
		.clickLeads();
		
		new MyLeadsPage()
		.clickMergeLead();
		
		
		MergeLeadPage MLP = new MergeLeadPage();
		MLP.clickFromWindow();
		
		FindLeadsPage FLP  = new FindLeadsPage();
		FLP.enterFindLeadsId(fromLeadId)
		.clickFindLeadButton()
		.clickFirstLink();
		
		MLP.clickToWindow();
		
		FLP
		.enterFindLeadsId(toLeadId)
		.clickFindLeadButton()
		.clickFirstLink();
		
		MLP.clickMergeLeadButton()
		.clickFindLeadsTab();
		
		FLP
		.enterFindLeadsId(fromLeadId)
		.clickFindLeadButton()
		.verifyerrMsgMergeLead();
		
		
	}
	@DataProvider(name="fetchData")
	public  Object[][] getData(){
		return DataInputProvider.getSheet(dataSheetName);		
	}	


}
