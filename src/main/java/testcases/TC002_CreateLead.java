package testcases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import pages.CreateLeadPage;
import pages.HomePage;
import pages.LoginPage;
import pages.MyHomePage;
import pages.MyLeadsPage;
import pages.ViewLeadPage;
import utils.DataInputProvider;
import wdMethods.ProjectMethods;

public class TC002_CreateLead extends ProjectMethods{
	@BeforeTest
	public void setData() {
		testCaseName = "TC001_CreateLeads";
		testDescription = "CreateLeads";
		authors = "sarath";
		category = "smoke";
		dataSheetName = "Input";
		testNodes = "CreateLeads";
	}
	
	@Test(dataProvider = "fetchData")
	public void createLead(String userName, String password,String compName,String firstName,String lastName) {		
		new LoginPage()
		.enterUserName(userName)
		.enterPassword(password)
		.clickLogin();
		
		new HomePage()
		.clickCRMSFA();
		
		new MyHomePage()
		.clickLeads();
		
		new MyLeadsPage()
		.clickCreateLeads();
		
		
		new CreateLeadPage()
		.enterCompanyName(compName)
		.enterFirstName(firstName)
		.enterLastName(lastName)
		.clickSubmit();
		
		new ViewLeadPage()
		.verifyFirstName(firstName);
	}
	@DataProvider(name="fetchData")
	public  Object[][] getData(){
		return DataInputProvider.getSheet(dataSheetName);		
	}	

}
