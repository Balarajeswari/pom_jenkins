package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import wdMethods.ProjectMethods;

public class CreateLeadPage extends ProjectMethods{
	@FindBy(id = "createLeadForm_companyName") WebElement eleCompanyName;
	@FindBy(id = "createLeadForm_firstName") WebElement eleFirstName;
	@FindBy(id = "createLeadForm_lastName") WebElement eleLastName;
	@FindBy(name = "submitButton") WebElement eleSubmit;
	public CreateLeadPage()
	{
		PageFactory.initElements(driver, this);
	}
	
	
	
	public CreateLeadPage enterCompanyName(String data)
	{
		type(eleCompanyName,data);
		return this;
	}
	public CreateLeadPage enterFirstName(String data)
	{
		type(eleFirstName,data);
		return this;
	}
	public CreateLeadPage enterLastName(String data)
	{
		type(eleLastName,data);
		return this;
	}
	public ViewLeadPage clickSubmit()
	{
		click(eleSubmit);
		return new ViewLeadPage();
	}
	
}
