package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import wdMethods.ProjectMethods;

public class MergeLeadPage extends ProjectMethods{
	public MergeLeadPage()
	{
		PageFactory.initElements(driver, this);
	}
	@FindBy(xpath = "//input[@name='partyIdFrom']/following::a") WebElement eleFromWindow;
	
	public FromToWindowPage clickFromWindow() {
	click(eleFromWindow);
	switchToWindow(1);
	return new FromToWindowPage();
	}
	
@FindBy(xpath = "//input[@name='partyIdTo']/following::a[1]") WebElement eleToWindow;
	
	public FromToWindowPage clickToWindow() {
	click(eleToWindow);
	switchToWindow(1);
	return new FromToWindowPage();
	}
	
	
	@FindBy(xpath = "//input[@name='partyIdTo']/following::a[2]") WebElement eleMergeLeadButton;
	public MergeLeadPage clickMergeLeadButton() throws InterruptedException {
		click(eleMergeLeadButton);
		acceptAlert();
		return this;
	}
	
	@FindBy(linkText = "Find Leads") WebElement eleFindLeadsTab;
	public FindLeadsPage clickFindLeadsTab() {
		click(eleFindLeadsTab);
		return new FindLeadsPage();
	}
	
}
