package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import wdMethods.ProjectMethods;

public class MyLeadsPage extends ProjectMethods {

	public MyLeadsPage()
	{
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(linkText = "Create Lead") WebElement eleCreateLead;
	public CreateLeadPage clickCreateLeads()
	{
		click(eleCreateLead);
		return new CreateLeadPage();
	}
	
@FindBy(linkText = "Merge Leads") WebElement eleMergeLead;
	
	
	public MergeLeadPage clickMergeLead() {
		click(eleMergeLead);
		return new MergeLeadPage();
	}
}
