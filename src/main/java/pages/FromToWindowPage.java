package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import wdMethods.ProjectMethods;

public class FromToWindowPage extends ProjectMethods{
	
	@FindBy(name = "id") WebElement eleFromLeadId;
	public FromToWindowPage enterFirstLeadId(String data) {
		type(eleFromLeadId,data);
		return this;
	}
	/*@FindBy(name = "id") WebElement eleToLeadId;
	public FromToWindowPage clickToLeadId(String data) {
		type(eleToLeadId,data);
		return this;
	}*/
	@FindBy(xpath = "//button[text()='Find Leads']") WebElement eleFindLeadButton;
	public FromToWindowPage clickFindLeadButton() {
		click(eleFindLeadButton);
		return this;
	}
	@FindBy(xpath = "//a[@class='linktext']") WebElement eleFirstLink;
	public MergeLeadPage clickFirstLink() throws InterruptedException {
		click(eleFirstLink);
		Thread.sleep(3000);
		switchToWindow(0);
		return new MergeLeadPage();
	}
}
