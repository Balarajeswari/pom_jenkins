package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import wdMethods.ProjectMethods;

public class FindLeadsPage extends ProjectMethods{
	public FindLeadsPage() {
		PageFactory.initElements(driver, this);
	}
	
	
	@FindBy(name = "id") WebElement eleFindLeadId;
	public FindLeadsPage enterFindLeadsId(String data) {
		type(eleFindLeadId,data);
		return this;
	}
	
	@FindBy(xpath = "//button[text()='Find Leads']") WebElement eleFindLeadButton;
	public FindLeadsPage clickFindLeadButton() {
		click(eleFindLeadButton);
		return this;
	}
	
	@FindBy(xpath = "//div[@class='x-paging-info']") WebElement errMsgMergeLead;
	public FindLeadsPage verifyerrMsgMergeLead() {
		verifyPartialText(errMsgMergeLead, "No");
		return this;
	}
	
	
	/*@FindBy(name = "id") WebElement eleFindLeads;
	public FindLeadsPage clickFindLeads() throws InterruptedException {
		click(eleFindLeads);
		Thread.sleep(3000);
		return this;
	}*/
	
	@FindBy(xpath = "//a[@class='linktext']") WebElement eleFirstLink;
	public MergeLeadPage clickFirstLink() throws InterruptedException {
		click(eleFirstLink);
		Thread.sleep(3000);
		switchToWindow(0);
		return new MergeLeadPage();
	}
	
	
}
