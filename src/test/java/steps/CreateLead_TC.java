package steps;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.chrome.ChromeDriver;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class CreateLead_TC {
	public ChromeDriver driver;
	@Given("Open The Browser")
	public void openTheBrowser() {
	    System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
	    driver = new ChromeDriver();
	}

	@Given("Max the browser")
	public void maxTheBrowser() {
	   driver.manage().window().maximize();
	}

	@Given("set the TimeOut")
	public void setTheTimeOut() {
	   driver.manage().timeouts().implicitlyWait(300,TimeUnit.SECONDS);
	}

	@Given("Launch the URL")
	public void launchTheURL() {
		driver.get("http://leaftaps.com/opentaps/control/main");
	}

	@Given("Enter the UserName as (.*)")
	public void enterTheUserNameAsDemoSalesManager(String data) {
		driver.findElementById("username").sendKeys(data);
	}

	@Given("Enter the Password as (.*)")
	public void enterThePasswordAsCrmsfa(String data) {
		driver.findElementById("password").sendKeys(data);
	}

	@When("Click on the Login Button")
	public void clickOnTheLoginButton() {
		driver.findElementByClassName("decorativeSubmit").click();
	}

	@Then("Verify the Login")
	public void verifyTheLogin() {
	   System.out.println("Login Successful");
	}

	@Then("Click on the CRMSFA Link")
	public void clickOnTheCRMSFALink() {
		driver.findElementByLinkText("CRM/SFA").click();
	}

	@Given("Open The Leads Tab")
	public void openTheLeadsTab() {
	    driver.findElementByLinkText("Leads").click();
		
	}

	@Given("Open The CreateLeads Tab")
	public void openTheCreateLeadsTab() {
		driver.findElementByLinkText("Create Lead").click();
	}

	@Given("Enter the Company Name as (.*)")
	public void enterTheCompanyNameAsAccenture(String data) {
		driver.findElementById("createLeadForm_companyName").sendKeys(data);
	}

	@Given("Enter the First Name as (.*)")
	public void enterTheFirstNameAsBala(String data) {
		
		driver.findElementById("createLeadForm_firstName").sendKeys(data);
	}

	@Given("Enter the Last Name as (.*)")
	public void enterTheLastNameAsShankar(String data) {
		driver.findElementById("createLeadForm_lastName").sendKeys(data);
	}

	@When("Click on the Create Lead Button")
	public void clickOnTheCreateLeadButton() {
		driver.findElementByName("submitButton").click();
	}

	@Then("Verify the First Name as (.*)")
	public void verifyTheFirstName(String data) {
		String text = driver.findElementById("viewLead_firstName_sp").getText();
		if(text.equals(data))
		{
			System.out.println("True");
		}
		else
		{
			System.out.println("False");
		}
	}



}
