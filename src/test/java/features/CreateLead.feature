Feature: Login to LeafTaps and Create Lead

Background:
Given Open The Browser
And Max the browser
And set the TimeOut
And Launch the URL



Scenario Outline: Successful Create Lead
And Enter the UserName as <userName>
And Enter the Password as <password>
When Click on the Login Button
Then Verify the Login
And Click on the CRMSFA Link
Given Open The Leads Tab
Given Open The CreateLeads Tab
And Enter the Company Name as <companyName>
And Enter the First Name as <firstName>
And Enter the Last Name as <lastName>
When Click on the Create Lead Button
Then Verify the First Name as <firstName>

Examples:
|userName|password|companyName|firstName|lastName|
|DemoSalesManager|crmsfa|Accenture|Bala|Shankar|
|DemoSalesManager|crmsfa|Accenture|Ram|Swathi|

#Feature: Login to LeafTaps and Create Lead
#
#Scenario: Positive Login and Successful Create Lead
#Given Open The Browser
#And Max the browser
#And set the TimeOut
#And Launch the URL
#And Enter the UserName as <userName>
#And Enter the Password as <password>
#When Click on the Login Button
#Then Verify the Login
#And Click on the CRMSFA Link
#Given Open The Leads Tab
#Given Open The CreateLeads Tab
#
#
#And Enter the Company Name as <companyName>
#And Enter the First Name as <firstName>
#And Enter the Last Name as <lastName>
#When Click on the Create Lead Button
#Then Verify the First Name as Bala